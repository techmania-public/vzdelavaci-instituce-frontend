import $ from 'jquery';
import 'what-input';

// Foundation JS relies on a global varaible. In ES6, all imports are hoisted
// to the top of the file so if we used`import` to import Foundation,
// it would execute earlier than we have assigned the global variable.
// This is why we have to use CommonJS require() here since it doesn't
// have the hoisting behavior.
window.jQuery = $;

// Zvolte, které kousky foundationu budeme importovat.
import './lib/foundation-explicit-pieces';


$(document).foundation();

// Jednotlivé kousky kódu
import './pieces/title-bar-menu.js';
import './pieces/main-searchfield.js';
import './pieces/quick-links.js';
import './pieces/facets.js';
import './pieces/lazyload.js';
import './pieces/mm.js';
import './pieces/facets-mobile-collapse.js';
import './pieces/magnific-popup.js';

// Tento kód je jen prů účely demonstrace overlaye.
// V produkční verzi jej prosím odstraňte.
import './pieces/facets-overlay.js';

// import './pieces/wow.js'
