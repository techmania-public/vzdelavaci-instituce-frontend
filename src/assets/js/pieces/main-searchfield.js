import $ from "jquery";

/**
 * Once the focus is on one of child elements,
 * add "drop-shadow" class to the selected parent.
 * 
 * Names of all the classes are to be customized below.
 * 
 * @author Jan Jáchim
 * 
 */

 $(document).ready(function(){

    const shadowTriggerClassName = "shadow-on-focus";
    const shadowCasterClassName = "shadow-drop-on-focus";
    const activeClassName = "drop-shadow";

    $("."+shadowTriggerClassName).on("blur",(e)=>{

        let caster = $( e.target ).closest( "."+shadowCasterClassName );

        if ( e.relatedTarget == null ) {
            caster.removeClass(activeClassName);
            return;
        }

        if ( !e.relatedTarget.classList.contains(shadowTriggerClassName) ) {
            caster.removeClass(activeClassName);
            return;
        }

    }).on("focus",(e)=>{
        let caster = $( e.target ).closest( "."+shadowCasterClassName );
        caster.addClass(activeClassName);
    });

 });