import $ from "jquery";



$(document).ready(function(){

    const triggerClass = ".ff-mobile-trigger";
    const wrapperClass = ".ff";
    const expandedClass = "ff_mobile-expanded";
    
    const wrapper = $(wrapperClass);

    $(triggerClass).on("click", function(){
        wrapper.toggleClass(expandedClass);
    });

});