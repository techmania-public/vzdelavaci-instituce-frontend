import $ from "jquery";

import "magnific-popup";

$(document).ready(function(){

		const images = $(".gallery-link");
		if ( images.length > 0 ) {

			images.magnificPopup({
				type: 'image',
				closeOnContentClick: true,
				mainClass: 'mfp-img-mobile',
				image: {
					verticalFit: true
						},
						gallery: {
					enabled: true,
					navigateByImgClick: true,
					preload: [0,1] // Will preload 0 - before current, and 1 after the current image
				},
			});
			
		}
    
		
	
});