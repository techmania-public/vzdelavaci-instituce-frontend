import $ from "jquery";

$( document ).ready(function(){

    const ff = $(".ff-group");
    const activeClass = "ff-group_expanded";

    ff.each((i,item)=>{
        const trigger = $(item).find(".ff-group-title");
        trigger.on("click",function(){
            $(item).toggleClass(activeClass);
        });
    });

});