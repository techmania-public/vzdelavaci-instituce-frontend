import $ from "jquery";

const activeClass = "is-active";
const triggerClass = ".quick-link-trigger";
const filterGroupClass = ".quick-filter-group";
const filterGroups = $( filterGroupClass );
const triggerGroup = $( triggerClass );

$(document).ready(function(){
    $(triggerClass).on("click",function(){
        let target = $(this).attr("data-target");
        if (target) {
    
            if ($( target ).hasClass(activeClass) ) {
    
                $( target ).removeClass( activeClass );
                $( this ).removeClass( activeClass );
    
            } else {
                triggerGroup.each(function(){
                    if ( $(this).attr("data-target") === target ){
                        $(this).addClass( activeClass );
                    } else {
                        $(this).removeClass( activeClass );
                    }
                });
                filterGroups.each(function(){
                    // console.log(target.slice(1));
                    if ( $(this).attr("id")==target.slice(1) ){
                        $(this).addClass(activeClass);
                    } else {
                        $(this).removeClass(activeClass);
                    }
                });
            }
        }
    });
});