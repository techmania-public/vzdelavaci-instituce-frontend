import $ from "jquery";

// Nastavení



/**
 * Blokování konkurentních dotazů
 * 
 * Jakmile je spuštěn nový dotaz, je nutné až do jeho zpracování
 * zamezit spouštění dalších paralelních dotazů. 
 * 
 * Blokování se děje těmito prostředky:
 * - při spuštění dotazu okamžitě spustit markLoadingStart()
 * - při dokončení zpracování dotazu spustit markLoadingEnd()
 * 
 * Obě metody dělají následující:
 * - nastaví proměnnou ffIsLoading, která na úrovni JS blokuje 
 *   spouštění nových dotazů
 * - Všem elementům obsahující overlay element nastaví třídy,
 *   kterými se překryv zobrazí a znemožní další klikání na 
 *   elementy jimiž se dotazy spouští
 * 
 */
let ffIsLoading = false;

// Instance dotyčných objektů
const ffOverlay = $(".ff-overlay"); // Instance všech overlayú
const ffElements = $(".ff-link-a"); // instance všech aktivních odkazů

const overlayActiveClass = "ff-overlay_active"; // aktivní třída overlayů

/* 
    Event při kliknutí na element ve filtru.
    V produkční verzi tento event nejspíše smažete
    a ve vlastním skriptu použijete teprve funkce
    markLoadingStart() a markLoadingEnd().
*/
$(document).ready(function(){
    ffElements.each((i,item)=>{
        $(item).on("click",function(e){
            e.preventDefault();
    
            // Pokračovat jedině když filtrování není aktivní
            if (!ffIsLoading) {
                
                // Označ začátek načítání
                markLoadingStart();
    
                // pro účelu náhledu je zde setTimeout()
                setTimeout(function(){ 
                    
                    // Zde bude pravděpodobně Váš AJAX

                    // Pro účely náhledu je zde následující kód, ale ten smažte.
                    const href= $(e.currentTarget).attr("href"); 
                    if ( href !== window.location.href ) {
                        window.location.href = href;
                    }

                    // Označ konec načítání
                    makrLoadingEnd();              
                    
                }, 1000);
            }
            
        })
    });
});


/**
 * Co vše se má stát, když začne loading
 */
export function markLoadingStart(){
    ffIsLoading = true;
    ffOverlay.addClass(overlayActiveClass);
}
/**
 * Co vše se má stát, když se přestane načítat
 */
export function makrLoadingEnd(){
    ffIsLoading = false;
    ffOverlay.removeClass(overlayActiveClass);
}

