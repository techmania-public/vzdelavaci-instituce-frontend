import $ from 'jquery';

$(document).ready(function(){

    const rows = $(".mm-list-item");
    const expandatorClass = ".mm-expandator";
    const activeClass = "mm-row_expanded";
    
    if ( rows.length > 0 ) {
    
        // Rozklikávání responsivního menu
        rows.each(function(){
            const row = $(this);
            const exp = $(this).find(expandatorClass);
            exp.on("click",function(){
                row.toggleClass(activeClass);
            });
        });
    
    } 
});