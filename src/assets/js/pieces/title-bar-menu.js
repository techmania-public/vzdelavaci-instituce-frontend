import $ from "jquery";

/**
 * Přepínač mobilního menu v titlebaru
 */

$(document).ready(function(){

    const trigger = $(".title-bar-menu-trigger");
    const container = $(".styled-title-bar");

    trigger.on("click",(element)=>{
        container.toggleClass("expanded");
        $( element.currentTarget ).toggleClass("is-active");
    });
});