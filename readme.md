# Frontend vzdelavaci-instituce.cz

Kód je napsaný v Zurb Foundation v 3.5.1. Projekt byl scaffoldován pomocí `foundation-cli` (více [zde](https://github.com/foundation/foundation-cli)). 
Zdrojové soubory jsou ve složce `src/`, hotová šablona je ve složce `dist/`.

## Struktura souborů

```
dist/ - zde je zkompilovaný celý projekt
src/ - zdrojové soubory pro kompilaci HTML, CSS a JS
    data/ - yml soubory s daty (nepotřebujete)
    layouts/default.html - hlavní struktura html
    pages/ - co soubor to stránka v src/
    partials/ - jednotlivé elementy
    assets/
        js/ - zdrojové soubory JS
        scss/ - zdrojové soubory CSS v SCSS
        img/ - obrázky šablony
        fonts/ - písma šablony
```

## Práce s JS
Dost možná budete potřebovat vložit do šablony vlastní JS. Používáte-li webpack, nejlepší způsob je vytvořit nový soubor do složky `assets/js/pieces` a ten potom importovat v `assets/js/app.js`.

### AJAX související s filtrováním
Aktuálně jsou pro filtrování v šabloně odkazy a overlay je demonstrován provizorním skriptem se setTimeout() v `/src/assets/js/pieces/facets-overlay.js`. 

Pokud se Vám nebude chtít psát celý AJAX do výše uvedeného souboru, doporučuji přesto ve Vašem vlastním skriptu používat tyto funkce:

`markLoadingStart()` - zapne overlaye a spol.

`markLoadingEnd()` - vypne overlaye a spol.

Do vašeho skriptu si ty funkce naimportujete pomocí 
``` javascript
import { markLoadingStart, markLoadingEnd } from "./facets-overlay";
```
Jakmile budete mít funkce naimportované, pravděpodobně budete chtít smazat event v souboru `facets-overlay.js`. Vše v něm je zakomentované.

## Handlebars šablony
V šabloně je podsatatné, že kdekoliv vidíte `{{> nazev-partialu}}` znamená to, že se zobrazí soubor uložený někde v `partials/` pod názvem `nazev-partialu.html`.

## Práce s šablonou
Oficiální dokumentace Foundationu sice vypišuje spoustu vyfikundací, ale stačí následující:

- Instlace `npm install`
- Vývoj `npm run start`
- Build `npm run build`
- Servírovat hotovou verzi `npm run serve`

V produkční verzi nepoužívat soubory, které nebyly vybuilděné - jsou opravdu hodně nemimifikované a velké.